import * as firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";

//this config is being used for both development and production environment. Though, it is a best practice creating a second project and have two configs: one for production (prodConfig) and another for development (devConfig), so you choose the config based on the environment.
const config = {
  apiKey: "AIzaSyBDWbZr-wgBVt7Abb95o5VDd6rOfDxzL0M",
  authDomain: "authbiblioteca.firebaseapp.com",
  databaseURL: "https://authbiblioteca.firebaseio.com",
  projectId: "authbiblioteca",
  storageBucket: "authbiblioteca.appspot.com",
  messagingSenderId: "37234810034",
  appId: "1:37234810034:web:db33c0d487f32bb4407dab",
  measurementId: "G-514DP2DZE6"
};

if (!firebase.apps.length) {
  //initializing with the config object
  firebase.initializeApp(config);
}

//separting database API and authentication
const auth = firebase.auth();
const db = firebase.database();

const facebookProvider = new firebase.auth.FacebookAuthProvider();

export { db, auth, facebookProvider };
