import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Container } from "reactstrap";
import * as routes from "./constants/routes";
import Navigation from "./components/Navigation";
import LandingPage from "./components/Landing";
import SignUpPage from "./views/SignUp";
import SignInPage from "./views/SignIn";
import PasswordForgetPage from "./components/PasswordForget";
import HomePage from "./components/Home";
import AccountPage from "./components/Account";
import withAuthentication from "./components/withAuthentication";

const App = () => (
  <BrowserRouter>
    <Container>
      <Navigation />
      <Route exact path={routes.LANDING} component={LandingPage} />
      <Route path={routes.SIGN_IN} component={SignInPage} />
      <Route path={routes.SIGN_UP} component={SignUpPage} />
      <Route path={routes.PASSWORD_FORGET} component={PasswordForgetPage} />
      <Route path={routes.HOME} component={HomePage} />
      <Route path={routes.ACCOUNT} component={AccountPage} />
    </Container>
  </BrowserRouter>
);

export default withAuthentication(App); //using HoC to handle session
